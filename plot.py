#!/usr/bin/env python

import sys
if len(sys.argv)!=3:
    raise Exception("Usage: plotme.py cartesian_master.log cartesian_slave.log")


def load_data(filename):
    lines = open(filename).readlines()
    outgoing_data = []
    incoming_data = []
    for line in lines:
        entries = line.strip().split(' ')
        if(entries[0]=='O'):
            outgoing_data.append([float(x) for x in entries[1:]])
        elif(entries[0]=='I'):
            incoming_data.append([float(x) for x in entries[1:]])
        else:
            pass
    return (outgoing_data,incoming_data)

[master_outgoing_data, master_incoming_data] = load_data(sys.argv[1])
print("Master incoming data size {}".format(len(master_incoming_data)))
print("Master outgoing data size {}".format(len(master_outgoing_data)))
[slave_outgoing_data, slave_incoming_data] = load_data(sys.argv[2])
print("Slave incoming data size {}".format(len(slave_incoming_data)))
print("Slave outgoing data size {}".format(len(slave_outgoing_data)))

# Each sample looks like
# Packet_index Packet_timestamp (Reception_timestamp)

# Define start time under the assumption that the clocks are synchronized
start_time = master_incoming_data[0][1]

# Compute loss data for a window of 0.025 s
def compute_loss(outgoing_data,incoming_data,window_size=0.05):
    window_start = []
    window_loss = []
    t = outgoing_data[0][1]

    # build table which says if packet received
    success_list = dict([(x[1],False) for x in outgoing_data])
    for entry in incoming_data:
        success_list[entry[1]] = True

    while t < incoming_data[-1][1]:
        window_start.append(t)
        t_min = t
        t_max = t + window_size

        lost = 0
        packets = 0
        ## for each sent packet
        #for entry in outgoing_data:
        #    # try and find packet in incoming data
        #    if t_min<=entry[1] and entry[1]<t_max:
        #        packets = packets + 1
        #        if sum(p[1]==entry[1] for p in incoming_data)==0:
        #            lost = lost + 1

        # use table to obtain loss rate for window
        for entry in success_list.iteritems():
            if t_min<=entry[0] and entry[0]<t_max:
                packets = packets + 1
                if not entry[1]:
                    lost = lost + 1
        if packets:
            ratio = float(lost)/packets
        else:
            ratio = 0.
        window_loss.append(ratio)

        t = t_max
    return (window_start,window_loss)


[master2slave_loss_windows_50,master2slave_loss_data_50] = compute_loss(master_outgoing_data,slave_incoming_data,0.050)
[slave2master_loss_windows_50,slave2master_loss_data_50] = compute_loss(slave_outgoing_data,master_incoming_data,0.050)
[master2slave_loss_windows_500,master2slave_loss_data_500] = compute_loss(master_outgoing_data,slave_incoming_data,0.500)
[slave2master_loss_windows_500,slave2master_loss_data_500] = compute_loss(slave_outgoing_data,master_incoming_data,0.500)

t_master_coming = [x[1]-start_time for x in master_incoming_data]

import matplotlib.pyplot as plt

# use every n-th sample in data set
mst_subsample = 20
slv_subsample = 100

# cartesian translation velocity tracking plot
slv_offset = 2 + 6
mst_offset = 2
fig, ax = plt.subplots(3,1,sharex=True)
for plt_idx,direction in enumerate(['$v_x$','$v_y$','$v_z$']):
    ax[plt_idx].plot([x[1]-start_time for x in slave_outgoing_data[::slv_subsample]],[x[slv_offset+plt_idx] for x in slave_outgoing_data[::slv_subsample]],'r.',label="SLV x"+direction)
    ax[plt_idx].plot([x[1]-start_time for x in master_outgoing_data[::mst_subsample]],[x[mst_offset+plt_idx] for x in master_outgoing_data[::mst_subsample]],'bx',label="MST "+direction)
    #ax[plt_idx].legend()
    ax[plt_idx].set_ylabel(direction+' in m/s')
    ax[plt_idx].set_xlim(min(t_master_coming),max(t_master_coming))
ax[-1].set_xlabel('t in s')

plt.savefig("plot_0.png")
plt.show()


# timestamps + latency for SLV -> MST
fig, (ax1,ax2) = plt.subplots(2,1,sharex=True)
ax1.plot([x[1]-start_time for x in slave_outgoing_data[::slv_subsample]],[x[1]-start_time for x in slave_outgoing_data[::slv_subsample]],'r.',label="SLV out timestamp")
ax1.plot([x[1]-start_time for x in master_incoming_data[::slv_subsample]],[x[2]-start_time for x in master_incoming_data[::slv_subsample]],'bx',label="MST in timestamp")
ax1.legend()
ax1.set_ylabel('t in s')
ax1.set_xlim(min(t_master_coming),max(t_master_coming))


ax2.plot([x[1]-start_time for x in master_incoming_data[::slv_subsample]],[1000*(x[2]-x[1]) for x in master_incoming_data[::slv_subsample]],'bx',label="latency")
ax2.set_ylabel('\Delta in ms')
ax2.set_xlabel('t in s')
#ax2.set_yscale('log')
ax2.legend()

plt.savefig("plot_1.png")
plt.show()

# timestamps + latency for MST -> SLV
fig, (ax1,ax2) = plt.subplots(2,1,sharex=True)
ax1.plot([x[1]-start_time for x in master_outgoing_data[::mst_subsample]],[x[1]-start_time for x in master_outgoing_data[::mst_subsample]],'rx',label="MST out timestamp")
ax1.plot([x[1]-start_time for x in slave_incoming_data[::mst_subsample]],[x[2]-start_time for x in slave_incoming_data[::mst_subsample]],'b.',label="SLV in timestamp")
ax1.legend()
ax1.set_ylabel('t in s')

ax2.plot([x[1]-start_time for x in slave_incoming_data[::mst_subsample]],[1000*(x[2]-x[1]) for x in slave_incoming_data[::mst_subsample]],'bx',label="latency")
ax2.set_ylabel('\Delta in ms')
ax2.set_xlabel('t in s')
ax2.legend()

plt.savefig("plot_2.png")
plt.show()

# packet los for MST -> SLV and SLV -> MST
fig, (ax1,ax2) = plt.subplots(2,1,sharex=True,sharey=True)
ax1.plot([t-start_time for t in master2slave_loss_windows_50],[x*100 for x in master2slave_loss_data_50],label="w=50ms")
ax1.plot([t-start_time for t in master2slave_loss_windows_500],[x*100 for x in master2slave_loss_data_500],label="w=500ms")
ax1.set_xlim(min(t_master_coming),max(t_master_coming))
ax1.text(5,50,"Master to slave packet loss (including reordered packages)")
ax1.set_ylabel("Loss in %")
ax1.set_ylim([0.,100.])
ax1.legend()

ax2.plot([t-start_time for t in slave2master_loss_windows_50],[x*100 for x in slave2master_loss_data_50])
ax2.plot([t-start_time for t in slave2master_loss_windows_500],[x*100 for x in slave2master_loss_data_500])
ax2.text(5,50,"Slave to master packet loss (including reordered packages)")
ax2.set_ylabel("Loss in %")
ax2.set_xlabel("t in s")

plt.savefig("plot_3.png")
plt.show()


# latency histogram for MST -> SLV and SLV -> MST
# TODO: this plot shows any clock synchronization error clearly
fig, (ax1) = plt.subplots(1,1,sharex=True)

slv2mst_latencies = [1000*(x[2]-x[1]) for x in master_incoming_data]
mst2slv_latencies = [1000*(x[2]-x[1]) for x in slave_incoming_data]
global_min = min(slv2mst_latencies+mst2slv_latencies) * 0.99
print global_min
global_max = min(slv2mst_latencies+mst2slv_latencies) * 1.1
print global_max
bin_count = 25
from numpy import arange
bins = arange(global_min,global_max,(global_max - global_min)/bin_count)


ax1.hist(slv2mst_latencies,label="Slave to master",bins=bins)
ax1.hist(mst2slv_latencies,label="Master to slave",bins=bins,alpha=0.5)
ax1.set_yscale('log')
ax1.legend()
ax1.set_xlabel("$t_\Delta$ in ms")

plt.savefig("plot_4.png")
plt.show()





